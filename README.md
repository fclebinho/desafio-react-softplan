# Desafio Softplan

[![Build Status](https://gitlab.com/fclebinho/desafio-react-softplan/badges/master/pipeline.svg?branch=master)](https://gitlab.com/fclebinho/desafio-react-softplan)

Desafio tem por objeto a demonstrar habilidades de uso das tecnologias apontadas abaixo.

  - redux
  - react-router
  - @testing-library/react
  
### Demonstração

[Demo](https://desafio-softplan.herokuapp.com/)

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ git clone https://gitlab.com/fclebinho/desafio-react-softplan.git
$ cd desafio-react-softplan
$ cp .env.example .env.local
$ yarn
$ yarn start
```

> **Nota:** O arquivo **.env.local** deve ser preenchido com as credenciais conforme orientação do serviço publicado.