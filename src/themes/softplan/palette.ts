export default {
  primary: {
    main: "#c0392b"
  },
  background: {
    default: "#bdc3c7"
  }
};
