import { createMuiTheme } from "@material-ui/core/styles";

import typography from "./typography";
import palette from './palette';

export default createMuiTheme({
  palette,
  typography,
  overrides: {
    MuiButton: {
      root: {
        borderRadius: 0
      },
      contained: {
        boxShadow: "none",
        "&:hover": {
          boxShadow: "none"
        }
      }
    }
  }
});
