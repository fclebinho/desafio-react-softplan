import service from "./service";

service.interceptors.request.use(config => {
  config.headers = { "Content-Type": "application/json" };
  config.params = {
    ...config.params,
    ts: process.env.REACT_APP_MARVEL_TS,
    apikey: process.env.REACT_APP_MARVEL_PUBLIC_KEY,
    hash: process.env.REACT_APP_MARVEL_HASH
  };
  return config;
});

export const getAll = (filter?: string) =>
  service.get("/v1/public/characters", {
    params: {
      name: filter
    }
  });
