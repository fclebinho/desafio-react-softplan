import React from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { Breadcrumbs, Link, Typography } from "@material-ui/core";

import { RootState } from "../../store/reducers";
import { CharactersGrid } from "../../components";

const Home = () => {
  const history = useHistory();
  const state = (state: RootState) => state.charactersState.characters;
  const characters = useSelector(state);

  const handleClick = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => history.push("/");

  return (
    <>
      <Breadcrumbs aria-label="breadcrumb">
        <Link color="inherit" onClick={handleClick}>
          Desafio Softplan
        </Link>
        <Link color="inherit" onClick={handleClick}>
          Characters
        </Link>
        <Typography color="textPrimary">Home</Typography>
      </Breadcrumbs>
      <CharactersGrid characters={characters} />
    </>
  );
};

export default Home;
