import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { Breadcrumbs, Link, Typography } from "@material-ui/core";

import { RootState } from "../../store/reducers";
import CharacterDetails from "./character-details";
import CharacterEdit from "./character-edit";

const Detail = (props: any) => {
  const history = useHistory();
  const state = (state: RootState) => state.charactersState.characters;
  const characters = useSelector(state);
  const { id } = props.match.params;

  const findCharacter = (id: any) =>
    characters.find(character => character.id == id);

  const [character, setCharacter] = useState<any | undefined>(
    findCharacter(id)
  );

  const handleClick = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => history.push("/");

  useEffect(() => {
    setCharacter(findCharacter(id));
  }, [characters, findCharacter, id]);

  return (
    <>
      <Breadcrumbs aria-label="breadcrumb">
        <Link color="inherit" onClick={handleClick}>
          Desafio Softplan
        </Link>
        <Link color="inherit" onClick={handleClick}>
          Characters
        </Link>
        <Typography color="textPrimary">
          {character && character.name}
        </Typography>
      </Breadcrumbs>
      <CharacterEdit character={character} />
      <CharacterDetails character={character} />
    </>
  );
};

export default Detail;
