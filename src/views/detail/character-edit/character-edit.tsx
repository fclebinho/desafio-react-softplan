import React, { useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

import CharacterForm from "./character-form";
import CharacterView from "./character-view";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: "24px 0"
    }
  })
);

interface IProps {
  character: any;
}

const CharacterEdit: React.FC<IProps> = ({ character }) => {
  const classes = useStyles();
  const [editing, setEditing] = useState(false);

  return (
    <div className={classes.root}>
      {editing ? (
        <CharacterForm
          onChange={() => setEditing(!editing)}
          character={character}
        />
      ) : (
        <CharacterView
          onClick={() => setEditing(!editing)}
          character={character}
        />
      )}
    </div>
  );
};

export default CharacterEdit;
