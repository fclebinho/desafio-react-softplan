import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Button, TextField } from "@material-ui/core";

import { editCharacter } from "../../../../store/actions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      alignItems: "center"
    },
    button: {
      marginLeft: 10
    }
  })
);

interface IProps {
  character: any;
  onChange?: () => void;
}

const CharacterForm: React.FC<IProps> = ({ character, onChange }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [state, setState] = useState(character);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setState({ ...state, name: event.target.value });

  const handleConfirm = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    dispatch(editCharacter(state));
    onChange && onChange();
  };

  return (
    <div className={classes.root}>
      <TextField
        id="outlined-basic"
        label="Character Name"
        variant="outlined"
        size="small"
        value={state.name}
        onChange={handleChange}
      />
      <div className={classes.button}>
        <Button onClick={handleConfirm}>Confirm</Button>
      </div>
    </div>
  );
};

export default CharacterForm;
