import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Button, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      alignItems: "center"
    },
    button: {
      marginLeft: 10
    }
  })
);

interface IProps {
  character: any;
  onClick: () => void;
}

const CharacterView: React.FC<IProps> = ({ character, onClick }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h3" component="h2">
        {character.name}
      </Typography>

      <div className={classes.button}>
        <Button onClick={onClick}>Edit</Button>
      </div>
    </div>
  );
};

export default CharacterView;
