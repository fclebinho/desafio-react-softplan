import { getAll } from "../../services";
import { Dispatch } from "redux";

import { ActionType, CharactersType } from "../../types";

export const REQUEST_CHARACTERS = "REQUEST_CHARACTERS";
export type REQUEST_CHARACTERS = typeof REQUEST_CHARACTERS;

export const RECEIVE_CHARACTERS = "RECEIVE_CHARACTERS";
export type RECEIVE_CHARACTERS = typeof RECEIVE_CHARACTERS;

export const EDIT_CHARACTER = "EDIT_CHARACTER";
export type EDIT_CHARACTER = typeof EDIT_CHARACTER;

const requestCharacters = (filter?: string): ActionType => {
  return {
    type: REQUEST_CHARACTERS,
    payload: filter
  };
};

const receiveCharacters = (data: CharactersType): ActionType => {
  return {
    type: RECEIVE_CHARACTERS,
    payload: data
  };
};

export const editCharacter = (character: any): ActionType => {
  return {
    type: EDIT_CHARACTER,
    payload: character
  };
};

export const fetchCharacters = (filter?: string) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(requestCharacters(filter));
    return getAll(filter).then(({ data }) => dispatch(receiveCharacters(data)));
  };
};
