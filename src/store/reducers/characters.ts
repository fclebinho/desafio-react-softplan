import {
  REQUEST_CHARACTERS,
  RECEIVE_CHARACTERS,
  EDIT_CHARACTER
} from "../actions";
import { ActionType, CharactersProps } from "../../types";

export const initialState = {
  characters: [],
  loading: false
};

export function charactersReducer(
  state: CharactersProps = initialState,
  action: ActionType
): CharactersProps {
  switch (action.type) {
    case REQUEST_CHARACTERS:
      return { ...state, loading: true };

    case RECEIVE_CHARACTERS:
      const { results: characters } = action.payload.data;

      return {
        ...state,
        characters,
        loading: false
      };

    case EDIT_CHARACTER:
      return {
        ...state,
        characters: state.characters.map(character =>
          character.id === action.payload.id ? action.payload : character
        ),
        loading: false
      };
    default:
      return state;
  }
}
