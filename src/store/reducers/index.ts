import { combineReducers } from "redux";

import { charactersReducer } from "./characters";

export const Reducers = combineReducers({
  charactersState: charactersReducer
});

export { initialState } from './characters';
export type RootState = ReturnType<typeof Reducers>