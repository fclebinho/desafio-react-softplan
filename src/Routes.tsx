import React from "react";
import { Switch } from "react-router-dom";

import { MainLayout } from './layouts';
import { RouteWithLayout } from './components';
import { HomeView, DetailView } from "./views";

const Routes = () => {
  return (
    <Switch>
      <RouteWithLayout 
        exact
        path="/"  
        layout={MainLayout}  
        component={HomeView} 
      />
      <RouteWithLayout 
        path="/detail/:id"  
        layout={MainLayout}  
        component={DetailView} 
      />
    </Switch>
  );
};

export default Routes;
