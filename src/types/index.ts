export interface CharactersProps {
  characters: any[];
  loading: boolean;
}

export interface StoreState {
  charactersState: CharactersProps;
}

export type CharactersType = CharactersProps; 

interface ActionProps {
  type: any;
  payload: any;
};
export type ActionType = ActionProps;