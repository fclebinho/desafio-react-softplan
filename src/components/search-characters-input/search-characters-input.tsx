import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  createStyles,
  fade,
  Theme,
  makeStyles
} from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import { CircularProgress } from "@material-ui/core";

import { RootState } from "../../store/reducers";
import { fetchCharacters } from "../../store/actions";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    search: {
      position: "relative",
      backgroundColor: fade(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: fade(theme.palette.common.white, 0.25)
      },
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(1),
        width: "auto"
      }
    },
    searchIcon: {
      width: theme.spacing(7),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    inputRoot: {
      color: "inherit"
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: 120,
        "&:focus": {
          width: 200
        }
      }
    }
  })
);

type AwaitingType = {
  filter: string;
  typingTimeout?: number;
};

const SearchCharactersInput = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const state = (state: RootState) => state.charactersState.loading;
  const loading = useSelector(state);
  const [searching, setSearching] = useState<AwaitingType>({
    filter: ""
  });

  const toSearch = (filter: string) => {
    if (filter === "") {
      dispatch(fetchCharacters());
      return;
    }

    dispatch(fetchCharacters(filter));
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (searching.typingTimeout) {
      clearTimeout(searching.typingTimeout);
    }

    setSearching({
      filter: event.target.value,
      typingTimeout: setTimeout(toSearch, 1000, event.target.value)
    });
  };

  const handleCharactersClick = () => {
    dispatch(
      fetchCharacters(searching.filter === "" ? undefined : searching.filter)
    );
  };

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        {loading ? (
          <CircularProgress color="inherit" size={20} />
        ) : (
          <SearchIcon onClick={handleCharactersClick} />
        )}
      </div>
      <InputBase
        placeholder="Search…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput
        }}
        inputProps={{ "aria-label": "search" }}
        value={searching.filter}
        onChange={handleInputChange}
      />
    </div>
  );
};

export default SearchCharactersInput;
