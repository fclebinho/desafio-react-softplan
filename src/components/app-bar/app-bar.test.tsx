import React from "react";
import { Provider } from "react-redux";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import configureStore, { preloadedState } from "../../store";
import AppBar from "./app-bar";

describe("AppBar", () => {
  let store;
  beforeEach(() => {
    store = configureStore(preloadedState);
  });

  describe("WHEN created component", () => {
    let getByLabelText, getByText, container;
    beforeEach(() => {
      ({ getByLabelText, getByText, container } = render(
        <Provider store={store}>
          <AppBar title="Desafio Softplan" />
        </Provider>
      ));
    });

    test("THEN there are no todos shown", () => {
      expect(container).toHaveTextContent(/Desafio Softplan/i);
    });
  });
});
