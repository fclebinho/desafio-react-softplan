import React from 'react';
import MuiAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';

import SearchCharactersInput from '../search-characters-input';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    title: {
      flexGrow: 1,
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    }
  }),
);

interface IProps {
  title: string;
}

const AppBar: React.FC<IProps> = ({ title }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <MuiAppBar position="fixed" elevation={0}>
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            {title}
          </Typography>
          <SearchCharactersInput />
        </Toolbar>
      </MuiAppBar>
    </div>
  );
}

export default AppBar;