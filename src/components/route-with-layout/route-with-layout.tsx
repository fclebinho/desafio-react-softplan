import React from 'react';
import { Route } from 'react-router-dom';

interface IProps {
  layout: React.ReactType;
  component: React.ReactType;
  path: string;
  exact?: any;
}

const RouteWithLayout: React.FC<IProps> = ({
  layout: Layout,
  component: Component,
  ...props
}) => {
  return (
    <Route
      {...props}
      render={matchProps => (
        <Layout>
          <Component {...matchProps} />
        </Layout>
      )}
    />
  );
};

export default RouteWithLayout;