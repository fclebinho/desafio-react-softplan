import React from "react";
import { Provider } from "react-redux";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import configureStore, { preloadedState } from "../../store";
import CharacterCard from "./character-card";

jest.mock("react-router-dom", () => ({
  useHistory: () => ({
    push: jest.fn()
  })
}));

describe("AppBar", () => {
  let store: any;
  let character: any;
  beforeEach(() => {
    store = configureStore(preloadedState);
    character = {
      id: 1011334,
      name: "3-D Man",
      thumbnail: {
        path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
        extension: "jpg"
      }
    };
  });

  describe("WHEN created component", () => {
    let getByLabelText, getByText, container;
    beforeEach(() => {
      ({ getByLabelText, getByText, container } = render(
        <Provider store={store}>
          <CharacterCard character={character} />
        </Provider>
      ));
    });

    test("THEN there are no todos shown", () => {
      expect(container).toHaveTextContent(/3-D Man/i);
    });
  });
});
