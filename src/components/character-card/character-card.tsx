import React from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography
} from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    maxWidth: 345
  }
});

interface IProps {
  character: any;
}

const CharacterCard: React.FC<IProps> = ({ character }) => {
  const history = useHistory();
  const classes = useStyles();

  const image = `${character.thumbnail.path}.${character.thumbnail.extension}`;

  return (
    <Card className={classes.root} variant="outlined">
      <CardActionArea onClick={() => history.push(`/detail/${character.id}`)}>
        <CardMedia
          component="img"
          alt={character.name}
          height="140"
          image={image}
          title={character.name}
        />
      </CardActionArea>
      <CardContent>
        <Typography gutterBottom variant="h6" component="h2">
          {character.name}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default CharacterCard;
