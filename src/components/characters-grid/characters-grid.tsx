import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";

import CharacterCard from "../character-card";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 15
    }
  })
);

interface IProps {
  characters: any[];
}

const CharactersGrid: React.FC<IProps> = ({ characters }) => {
  const classes = useStyles();

  return (
    <Grid className={classes.root} container spacing={3}>
      {characters.map((character, index) => (
        <Grid key={index} item xs={3}>
          <CharacterCard character={character} />
        </Grid>
      ))}
    </Grid>
  );
};

export default CharactersGrid;
