import React from "react";
import { Provider } from "react-redux";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import configureStore, { preloadedState } from "../../store";
import CharacterGrid from "./characters-grid";

jest.mock("react-router-dom", () => ({
  useHistory: () => ({
    push: jest.fn()
  })
}));

const characters = [
  {
    id: 1,
    name: "Hero 1",
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
      extension: "jpg"
    }
  },
  {
    id: 2,
    name: "Hero 2",
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
      extension: "jpg"
    }
  },
  {
    id: 3,
    name: "Hero 3",
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
      extension: "jpg"
    }
  }
];

describe("CharacterGrid", () => {
  let store: any;
  let character: any;
  beforeEach(() => {
    store = configureStore(preloadedState);
  });

  describe("WHEN created component", () => {
    let getByLabelText, getByText, container;
    beforeEach(() => {
      ({ getByLabelText, getByText, container } = render(
        <Provider store={store}>
          <CharacterGrid characters={characters} />
        </Provider>
      ));
    });

    test("THEN there are items shown", () => {
      expect(container).toHaveTextContent(/Hero 1Hero 2Hero 3/i);
    });
  });
});
