import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { fetchCharacters } from "../../store/actions";

interface IProps {
  children: React.ReactNode;
}

const App: React.FC<IProps> = ({ children }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCharacters());
  }, [dispatch]);

  return <>{children}</>;
};

export default App;
