import React from "react";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";
import { Provider } from "react-redux";

import configureStore, { preloadedState } from "./store";

import { softplan } from "./themes";
import Routes from "./Routes";
import { AppContainer } from "./containers";

const App = () => {
  return (
    <Provider store={configureStore(preloadedState)}>
      <ThemeProvider theme={softplan}>
        <AppContainer>
          <BrowserRouter>
            <Routes />
          </BrowserRouter>
        </AppContainer>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
