import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';

import { AppBar } from '../../components';

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: 88
  },
}))

interface IProps {
  children: React.ReactNode;
}

const Main: React.FC<IProps> = ({ children }) => {
  const classes = useStyles();

  return (
    <>
      <AppBar title="Desafio Softplan" />
      <Container>
        <main className={classes.container}>
          {children}
        </main>
      </Container>
    </>
  )
}

export default Main;